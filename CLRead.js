/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


        Author: Andreas Peters
		www:  https://www.andreas-peters.net

*/

load("functions.js");

var CLRead = Class({
	frame : "",
	scrollbar : "",
	key : "",
	file: "",
	
	initialize: function(file) {
		this.file = file;
		// Create the Lightbar Frame 
		this.frame = new Frame(4,8,75,13);
		this.scrollbar = new ScrollBar(this.frame);

		this.readArticle();
	},

	close: function() {
		this.scrollbar.close();
		this.frame.close();
	},

	show: function() {
		this.refresh();
		this.main();
	},

	refresh: function() {
		setBackground("main.ans");
		setFooter("Q)uit/Back                            by Andreas - www.andreas-peters.net");
		this.frame.invalidate();
	},

	render: function() {
		this.frame.cycle();
	        this.scrollbar.cycle();
	},

	update: function() {
	    switch(this.key) {
        	case KEY_UP:
            		this.frame.scroll(0,-1);
	           	 break;
	        case KEY_DOWN:
	            	this.frame.scroll(0,1);
	            	break;
    		}
	},

	readArticle: function() {
		fp = new File(this.file.directory);
		fp.open("r");
		string = fp.read();
		string = string.replace(/<.*/g,"");
		this.frame.putmsg("\1w"+string);
		this.frame.open();
		this.frame.scrollTo(0,0);
	},

	main: function() {
    	while((userInput = console.inkey(K_NOECHO, 5)) != "q") {
        	this.key = userInput;
        	this.update();
        	this.render();
        }
    }

});

