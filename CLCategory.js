/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


        Author: Andreas Peters
		www:  https://www.andreas-peters.net

*/

load("functions.js");


var CLCategory = Class({
	frame : "",
	tree : "",
	key : "",
	category: "",
	dir: "",
	
	initialize: function(dir) {
		this.dir = dir;
		// Create the Lightbar Frame 
		this.frame = new Frame(4,8,74,13);
		this.tree = new Tree(this.frame);


		this.setCategoryList();
	},

	show: function() {
		this.refresh();
		this.main();
	},

	refresh: function() {
		setBackground("main.ans");
		setFooter("Q)uit R)ead                           by Andreas - www.andreas-peters.net");
		this.frame.invalidate();
	},

	close: function() {
		this.tree.close();
		this.frame.close();
	},

	render: function() {
		this.frame.cycle();
	},

	update: function() {
   		switch(userInput) {
	       		case "r":
				var kind = this.category[this.tree.index].name.split(" | ");
				var res = "";
				switch (kind[0]) {
					case "D":
           					res = new CLCategory(this.category[this.tree.index].directory+"/*");
						break;
					case "A":
           					res = new CLRead(this.category[this.tree.index]);
						break;
				}
				if (res) {
					res.show();
					res.close();
					this.refresh();
				} else {
					console.writeln(kind[0]+"a");
				}
           			break;
	   		}       
		this.tree.getcmd(this.key);
	},


	/*
		Function:		showCategoryList
		Description:	Show all the categories
		Parameters:		
		Return:
	*/
	setCategoryList: function() {
		var dir = directory(this.dir);
		this.category = Array();
		var x = 0;
		var length = dir.length-1;
		for (var i=length; i >= 0;i--) {
			var name = dir[i].split("/");
			if (name[name.length-2]) {
				this.category[x++] = { "name": "D | "+name[name.length-2], "directory": dir[i] };			
				this.tree.addItem(format("%-55.55s",this.category[x-1].name),i);
			} 
			if (dir[i].indexOf("cmd?cmd") > 1) {
				var fp = new File(dir[i]);
	                        fp.open("r", true);
                        	var content = fp.readAll();
                        	var title = content[11];
                        	var date  = content[8];
				date = date.split(/\s+/);
                        	fp.close();
				if (title && date) {
					title = title.replace(/Subj:/," ");
					this.category[x++] = { "name": "A | "+title, "directory": dir[i] };			
					this.tree.addItem(format("%-55.55s %-10.10s",this.category[x-1].name, date[3]),i);
				}
			}
		}

		this.frame.open();
		this.tree.open();
	},


	main: function() {
		while((userInput = console.inkey(K_NOECHO, 5)) != "q") {
			this.key = userInput;
			this.update();
			this.render();
    
		}
	}
});

